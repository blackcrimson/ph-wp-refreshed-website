<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'prologiq_dev_phnewsite' );

/** Remote environment **/

// define( 'DB_USER', 'prologiq_blackdev' ); // MySQL database username */
// define( 'DB_PASSWORD', '@PrologiQ2906' ); /** MySQL database password */
// define( 'DB_HOST', '164.160.91.12' ); /** MySQL hostname */


/** Local environment **/

define( 'DB_USER', 'root' ); /** MySQL database username */
define( 'DB_PASSWORD', 'root' ); /** MySQL database password */
define( 'DB_HOST', 'localhost' ); /** MySQL hostname */



/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '^1Nhva9@r6@qFP:lJ|>LJ2;bxZ>?^8|%%1e($A+?lbR *9CpZ9>X?q6T4:j]CJgl' );
define( 'SECURE_AUTH_KEY',  '_ALr8Vzf{2 _nRPBypt{:_^0*u~tvy9KsZX&3A%]:oa!.]&ZzKON2.YFn)VM9+|[' );
define( 'LOGGED_IN_KEY',    'ximVn{Uo@p }RsJ&#P.4%~ow?IzK#o]1jwB~?;YWR{1G:;dOOC)wo#/@,tNq[48c' );
define( 'NONCE_KEY',        'U~s1f>BQ2o/TJ1MZ8*RGkc7T,O$9~j0%lGK7HC9;X]  #>o7vlGk/yMGJ6pNO+R1' );
define( 'AUTH_SALT',        '1M&5K}/d#Vi|TZLZn^ZLfu^ 5c?gy,)kz}7 BE(Ug%S&z/7N:FrboMQR[A^J!*d}' );
define( 'SECURE_AUTH_SALT', '4r<%.VUDZO-m2T#Q.P~ k9[M p,!xbvQtbD|Ox_<o6B<g<n7W58-oSNd%`vV-USl' );
define( 'LOGGED_IN_SALT',   'L$ 3`gT[^v-x+VCBIG9g.VfB)2<c%A,U<z|lWIjZAOQ(Er|{FGegs:n?o^]N$h[`' );
define( 'NONCE_SALT',       'mJ@7s6pTy?G&G.y(.ldjln`!Ln<@RqSb-~QUSV^8M[CqXwQ(]8=0ABcTErI,.X>t' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'phps_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
